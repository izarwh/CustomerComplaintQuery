alter table consumercomplaints_1
add primary key (`Complaint ID`);

# select count(*) as `Jumlah Complaint`, DATE(`Date Received`) as `tanggal diterima`  from consumercomplaints_1
# group by MONTH(`Date Received`)
# order by MONTH(`Date Received`);
#
# select company, count(`Company Response to Consumer`) as 'Untimely response', count(`Company Response to Consumer`) as closed from consumercomplaints_1
# where `company response to consumer` like 'Untimely response' or `company response to consumer` like 'Closed'
# group by company;

select company,
       count(IF (`Company Response to Consumer` = 'Closed',Company,NULL)) as 'Closed',
       count(IF (`Company Response to Consumer` = 'Closed with explanation',Company,NULL)) as 'Closed with explanation',
       count(IF (`Company Response to Consumer` = 'Closed with non-monetary relief',Company,NULL)) as 'Closed with non-monetary relief'
from consumercomplaints_1
group by company;

# # group by `company response to consumer`
#
# select company from consumercomplaints_1
# where company like '1st Franklin Financial Corporation';

